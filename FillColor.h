#ifndef GRAPHICS2D_FILLCOLOR_H
#define GRAPHICS2D_FILLCOLOR_H

#include <SDL.h>
#include "Vector2D.h"
#include "Line.h"

void BoundaryFill4(SDL_Window *win, Vector2D startPoint,Uint32 pixel_format,
                     SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor);
void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor);
//void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor);
//void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor);
//void FillIntersection(Vector2D v1, Vector2D v2, int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor);

#endif

