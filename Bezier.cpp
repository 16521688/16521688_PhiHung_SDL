#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float x = 0;
	float y = 0;
	for (float t = 0; t <= 1; t += 0.001)
	{
		x = p1.x *(1 - t)*(1 - t) + 2 * p2.x*(1 - t)*t + p3.x *t*t;
		y = p1.y *(1 - t)*(1 - t) + 2 * p2.y*(1 - t)*t + p3.y *t*t;
		SDL_RenderDrawPoint(ren, int(x) + 0.5, int(y) + 0.5);
	}
	

}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	float x = 0;
	float y = 0;

	for (float t = 0; t <= 1; t += 0.001)
	{
		x = p1.x * (1 - t)*(1 - t)*(1 - t) + 3 * p2.x * (1 - t)*(1 - t) * t + 3 * p3.x * (1 - t)*t*t + p4.x*t*t*t;
		y = p1.y * (1 - t)*(1 - t)*(1 - t) + 3 * p2.y * (1 - t)*(1 - t) * t + 3 * p3.y * (1 - t)*t *t + p4.y*t *t*t;
		SDL_RenderDrawPoint(ren, int(x) + 0.5, int(y) + 0.5);
	}
}


