#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);

}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{

	//Area 1
	int x = 0;
	int y = b;
	int p = -2 * a*a*b + a * a + 2 * b*b;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw4Points(xc, yc, x, y, ren);


	while (x*x*(a*a + b * b) <= a * a*a*a)
	{
		if (p <= 0) {
			p += 4 * b*b*x + 6 * b*b;
		}
		else
		{
			y--;
			p += 4 * b*b*x - 4 * a*a*y + 4 * a*a + 6 * b*b;
		}
		x += 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw4Points(xc, yc, x, y, ren);


	}

	//Area 2
	x = a;
	y = 0;
	p = b * b + 2 * a*a - 2 * a*b*b;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw4Points(xc, yc, x, y, ren);

	while (x*x*(a*a + b * b) > a * a*a*a)
	{
		if (p <= 0) {
			p += 4 * a*a*y + 6 * a*a;
		}
		else {
			p += 4 * a*a*y - 4 * b*b*x + 4 * b*b + 6 * a*a;
			x--;
		}
		y = y + 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw4Points(xc, yc, x, y, ren);

	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0;
	int y = b;
	float p = b * b - a * a*b + a * a / 4;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw4Points(xc, yc, x, y, ren);

	while (x*x*(a*a + b * b) <= a * a*a*a)
	{
		if (p <= 0)
		{
			p += 2 * b*b*x + 3 * b*b;
		}
		else
		{
			p += 2 * b*b*x - 2 * a*a*y - 2 * a*a + 3 * b*b;
			y = y - 1;
		}
		x = x + 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw4Points(xc, yc, x, y, ren);


	}

	// Area 2
	p = a * a - a * b*b + b * b / 4;
	x = a;
	y = 0;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b * b) > a*a*a*a)
	{
		if (p <= 0)
		{
			p += a * a*(2 * y + 3);
		}
		else
		{
			x--;
			p += a * a*(2 * y + 3) + b * b*(-2 * x + 2);
		}
		y++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw4Points(xc, yc, x, y, ren);
	}

}